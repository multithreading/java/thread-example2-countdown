package com.honor;

public class Main {

	public static void main(String[] args) {
		CountDown countDown = new CountDown();
		CountDownThread t1 = new CountDownThread(countDown);
		t1.setName("Thread1");

		CountDownThread t2 = new CountDownThread(countDown);
		t2.setName("Thread2");

		//t1.start();
		//t2.start();

		SyncCountDown synccountDown = new SyncCountDown();
		SyncCountDownThread st1 = new SyncCountDownThread(synccountDown);
		st1.setName("Thread1");

		SyncCountDownThread st2 = new SyncCountDownThread(synccountDown);
		st2.setName("Thread2");

		st1.start();
		st2.start();
	}
}

class SyncCountDown {

	private int i;
	/*
	 * Methods can be defined as "synchronised" When a method is declared as
	 * "synchronised" that method can only be executed by 1 thread at a time. the
	 * thread that executes the method won't be suspended until the method execution
	 * is complete. This prevents thread interference.
	 * 
	 * If a class has 3 synchronised methods only one of them can be running at a
	 * time and by only 1 thread.
	 * 
	 * be careful, race condition can be prevented within synchronised methods but
	 * not outside of those methods. if the same instance variable referenced in
	 * synchronised method are also referenced outside in another code piece which
	 * is not synchronised and can be executed by multiple threads, then we would
	 * still be subject to thread interference/race condition.
	 * 
	 * in this class doCountdown method is declared as synchronized and so although
	 * we are using an instance variable inside the for loop we are seeing no thread
	 * interference.
	 * 
	 * we can also synchronise a block of statements instead of an entire method.
	 * every java object has an intrinsic lock (monitor). we can synchronize a block
	 * of statements that work with an object by forcing the thread to acquire the
	 * object's lock before they execute the statement(s). only 1 thread can hold
	 * the lock so other threads that want the lock will be suspended until the
	 * current thread releases it.
	 * 
	 * using local variable for synchronization won't work. try sychronizing the for
	 * loop using "color" local variable and observe (remember use of thread stack
	 * vs heap). local "object" variables' references are stored in the thread stack
	 * (object values are stored in the heap but this won't result in interference
	 * since different references will be referencing different instances). thread
	 * stack will only hold primitive values and object references.
	 * 
	 */

	// public synchronized void doCountdown() {
	public void doCountdown() {
		String color;

		switch (Thread.currentThread().getName()) {
		case "Thread1":
			color = TColors.ANSI_BLUE;
			break;
		case "Thread2":
			color = TColors.ANSI_GREEN;
			break;
		default:
			color = TColors.ANSI_BLACK;
		}
		
		/*synchronizing a block of statements*/
		// synchronized (color) -> try and observe
		synchronized (this) {
			for (i = 20; i > 0; i--) {
				System.out.println(color + Thread.currentThread().getName() + ": i=" + i);
			}
		}
	}
}

class CountDown {

	private int ii;

	public void doCountdown() {
		String color;

		switch (Thread.currentThread().getName()) {
		case "Thread1":
			color = TColors.ANSI_BLUE;
			break;
		case "Thread2":
			color = TColors.ANSI_GREEN;
			break;
		default:
			color = TColors.ANSI_BLACK;
		}
		/*
		 * below i is a local variable. local variables are created in thread stack.
		 * each thread has its own thread stack and they can't access each other's
		 * stack.
		 */

		for (int i = 20; i > 0; i--) {
			System.out.println(color + Thread.currentThread().getName() + ": i=" + i);
		}

		/*
		 * if instead of using a local i variable we use an instance variable in the for
		 * loop, then both threads will have access to the same instance variable
		 * because instance variables are stored in the heap memory and heap is
		 * accessable by all threads.
		 *
		 * commentout the loop above and enable the loop below to see what happens.
		 *
		 * Note: the for loop can be suspended just before decrementing the loop
		 * variable, just before checking the condition or just before printing out to
		 * the console. (actually there are more suspension points considering the
		 * concatenation operations. the point is there are multiple points the current
		 * thread can be suspended.)
		 *
		 * Keep this in mind cause this will explain the 2 thread printing the same
		 * output to the console in some cases. For example if thread1 is suspended
		 * before decrementing the value of loop variable, thread2 will print the same
		 * value to the console.
		 *
		 * this is called thread interference or race condition.
		 */

		/*
		 * for(ii=20;ii>0;ii--){ System.out.println(color +
		 * Thread.currentThread().getName() + ": ii=" + ii); }
		 */

	}
}

class CountDownThread extends Thread {

	private CountDown threadCountDown;

	public CountDownThread(CountDown countDown) {
		this.threadCountDown = countDown;
	}

	public void run() {
		threadCountDown.doCountdown();
	}
}

class SyncCountDownThread extends Thread {
	private SyncCountDown syncCD;

	public SyncCountDownThread(SyncCountDown sync) {
		this.syncCD = sync;
	}

	public void run() {
		syncCD.doCountdown();
	}
}